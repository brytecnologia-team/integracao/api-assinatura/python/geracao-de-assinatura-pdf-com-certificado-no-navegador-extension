from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from flask_cors import CORS
import json
import requests
import base64

app = Flask(__name__)
CORS(app)
api = Api(app)

# Token de autenticação gerado no BRy Cloud
AUTHORIZATION = 'authorization'


class Inicializa_Assinatura(Resource):
    def post(self):
        # Pega o conteúdo que vem da requisição do front-end
        data = request.form

        # Lê o documento enviado para assinatura no front-end
        pdfParaASsinatura = request.files['documento'].read()

        # Verifica se a assinatura possuí imagem e realiza a requisição de acordo com essa condição
        if data['assinaturaVisivel'] == "true":
            image = request.files['imagem'].read()
            signer_form = {
                'dados_inicializar': '{ "certificado" : "' + data['certificado'] + '", "algoritmoHash" : "' + data['algoritmoHash'] + '", "perfil" : "' + data['perfil'] + '", "formatoDadosEntrada" : "BASE64", "formatoDadosSaida" : "BASE64", "nonces" : [""] }',
                'configuracao_imagem': '{"altura" : ' + data['altura'] + ',  "largura" : ' + data['largura'] + ',  "coordenadaX" : ' + data['coordenadaX'] + ',  "coordenadaY" : ' + data['coordenadaY'] + ',  "posicao" : "' + data['posicao'] + '", "pagina" : "' + data['pagina'] + '"}',
                'configuracao_texto': '{"texto" : "' + data['texto'] + '" ,"incluirCN" : ' + data['incluirCN'] + ', "incluirCPF" : ' + data['incluirCPF'] + ', "incluirEmail" : ' + data['incluirEmail'] + '}'
            }
            files = [
                ('documento', pdfParaASsinatura),
                ('imagem', image)
            ]
        else:
            signer_form = {
                'dados_inicializar': '{ "certificado" : "' + data['certificado'] + '", "algoritmoHash" : "' + data['algoritmoHash'] + '", "perfil" : "' + data['perfil'] + '", "formatoDadosEntrada" : "BASE64", "formatoDadosSaida" : "BASE64", "nonces" : [""] }',
            }
            files = [
                ('documento', pdfParaASsinatura),
            ]
        header = {
            'Authorization': AUTHORIZATION,
        }
        print(
            '============================================================================================')

        print('============= Inicializando assinatura PDF no BRy HUB utilizando certificado no navegador ... =============')

        response = requests.post('https://hub2.bry.com.br/fw/v1/pdf/pkcs1/assinaturas/acoes/inicializar',
                                 data=signer_form, files=files, headers=header)
        if response.status_code == 200:
            print('Assinatura inicializada com sucesso!')
            print(response.json())
            print('Processo de inicialização da assinatura concluido com sucesso')
            print(
                '============================================================================================')
            return response.json()
        else:
            print(response.text)


class Finaliza_Assinatura(Resource):
    def post(self):
        # Pega o conteúdo que vem da requisição do front-end
        data = request.form
        # Cria json para ser enviado na requsicao de finalização, os dados recebidos pelo front-end são os retornados após cifragem utilizando BRy Extension
        signer_form = '{"nonce": "' + data['nonce'] + '", "formatoDeDados": "' + data['formatoDeDados'] + '", "assinaturasPkcs1": [{"cifrado": "' + \
            data['cifrado'] + '", "nonce": "' + \
            data['insideNonce'] + '"}]}'
        header = {
            'Content-Type': 'application/json'
        }

        print('============= Finalizando assinatura PDF no BRy HUB utilizando certificado no navegador ... =============')

        response = requests.post('https://hub2.bry.com.br/fw/v1/pdf/pkcs1/assinaturas/acoes/finalizar',
                                 data=signer_form, headers=header)
        if response.status_code == 200:
            print('Assinatura finalizada com sucesso!')
            print(response.json())
            print('Assinatura realizada com sucesso!')
            return response.json()
        else:
            print(response.text)


api.add_resource(Inicializa_Assinatura, '/assinador/inicializar')
api.add_resource(Finaliza_Assinatura, '/assinador/finalizar')

if __name__ == '__main__':
    app.run(host="localhost", port=8000, debug=True)
