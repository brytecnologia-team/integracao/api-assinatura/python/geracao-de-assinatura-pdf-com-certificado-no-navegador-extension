# API de Geração de Assinatura de PDF com o BRy Extension

Este é o exemplo backend de integração dos serviços da API de assinatura com clientes baseados em tecnologia Python, que utilizam a BRy Extension.

Este exemplo apresenta os passos necessários para a geração de assinatura de PDF utilizando-se do certificado instalado no computador.
  - Passo 1: Recebimento do conteúdo do certificado digital e do documento a ser assinado.
  - Passo 2: Inicialização da assinatura e produção dos artefatos assinaturasInicializadas.
  - Passo 3: Envio dos dados para cifragem no frontend.
  - Passo 4: Recebimento dos dados cifrados no frontend.
  - Passo 5: Finalização da assinatura e obtenção do artefato assinado.

### Tech

O exemplo utiliza das bibliotecas Python abaixo:
* [Requests] - HTTP for Humans!
* [Flask] - Flask (source code) is a Python Web framework designed to receive HTTP requests.
* [Flask-restful] - Flask-RESTful is an extension for Flask that adds support for quickly building REST APIs
* [Flask-Cors] - A Flask extension for handling Cross Origin Resource Sharing (CORS), making cross-origin AJAX possible.
* [Python] - Python 3.8

**Observação**

Esta API em [Python] deve ser executada juntamente com o [FrontEnd](https://gitlab.com/brytecnologia-team/integracao/api-assinatura/react/assinatura-pdf-extensao), desenvolvido em React, para que seja feita a cifragem dos dados com a extensão para web "BRy Extension".

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

Por esse motivo, é necessário configurar a BRy Extension no seu browser, assim como ter um certificado importado.

**Observação**

É necessário ter o [Python] e todas suas dependências instalados na sua máquina, além do [Composer] para a instalação das dependências e execução do projeto. Também é necessário executar esta aplicação juntamente com o frontend.

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável  |                   Descrição                   | Arquivo de Configuração |  
| --------- | --------------------------------------------- | ----------------------- |
| AUTHORIZATION | Access Token para o consumo do serviço (JWT). |     pdf_signer.py    |

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência e instale as dependências. Utilizamos o Python versão 3.8 e pip3 para a instalação das dependências.

Comandos:

Instalar Flask:

    -pip3 install Flask

Instalar Flask-Cors

    -pip3 install -U flask-cors

Instalar Flask-Restful:

    -pip3 install flask-restful

Executar programa:

    -python3 pdf_signer.py



   [Requests]: <https://pypi.org/project/requests/>
   [Flask]: <https://palletsprojects.com/p/flask/>
   [Flask-restful]:<https://flask-restful.readthedocs.io/en/latest/> 
   [Flask-Cors]: <https://flask-cors.readthedocs.io/en/latest/>
   [Python]: <https://www.python.org/downloads/release/python-380/>